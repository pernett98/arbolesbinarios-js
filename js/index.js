//Inicio
function iniciar() {
    var canvas = document.getElementById("myCanvas");
    contexto = canvas.getContext("2d");

    cantidadNodos = document.getElementById("numero");

    d1 = document.getElementById("dato1");
    b1 = document.getElementById("boton1");

    d2 = document.getElementById("dato2");
    b2 = document.getElementById("boton2");

    d3 = document.getElementById("dato3");
    b3 = document.getElementById("boton3");

    arbol = new ArbolBinarioAVL();
    for (var i = 0; i < 11; i++) {
        arbol.insertar(Math.floor(Math.random() * 100));
    }
    /*for (var i=0;i<=11;i++){
     arbol.insertar(i);
     }*/
    arbol.pintar(contexto, 700, 20);
    //arbol.limpiarFondo(contexto,0,0);

    b1.addEventListener("click", btnInsertar);
    b2.addEventListener("click", btnBuscar);
    b3.addEventListener("click", btnEliminar);
    cantidadNodos.innerHTML = arbol.contarNodosRecursivo(arbol.obtenerRaiz());

    var n = 2;
    var no = "no";
    while (n) {
        no += no;
        n -= 1;
    }
    console.log(n);

}

//Botones
function btnInsertar() {
    var dato = Math.floor(d1.value);
    arbol.insertar(dato);
    arbol.limpiarFondo(contexto, 0, 0);
    arbol.pintarFondoNodo(contexto, arbol.obtenerRaiz(), 700, 20, dato);
    arbol.pintar(contexto, 700, 20);
    cantidadNodos.innerHTML = arbol.contarNodosRecursivo(arbol.obtenerRaiz());
    setTimeout(function() {
        arbol.limpiarFondo(contexto, 0, 0);
        arbol.pintar(contexto, 700, 20);
    }, 3000);
};

function btnBuscar() {
    var dato = Math.floor(d2.value);
    arbol.limpiarFondo(contexto, 0, 0);
    arbol.pintarFondoNodo(contexto, arbol.obtenerRaiz(), 700, 20, dato);
    arbol.pintar(contexto, 700, 20);
    cantidadNodos.innerHTML = arbol.contarNodosRecursivo(arbol.obtenerRaiz());
    setTimeout(function() {
        arbol.limpiarFondo(contexto, 0, 0);
        arbol.pintar(contexto, 700, 20);
    }, 3000);
};

function btnEliminar() {
    var dato = Math.floor(d3.value);
    arbol.limpiarFondo(contexto, 0, 0);
    arbol.pintarFondoNodo(contexto, arbol.obtenerRaiz(), 700, 20, dato);
    arbol.pintar(contexto, 700, 20);
    cantidadNodos.innerHTML = arbol.contarNodosRecursivo(arbol.obtenerRaiz());
    setTimeout(function() {
        arbol.eliminar(dato);
        arbol.limpiarFondo(contexto, 0, 0);
        arbol.pintar(contexto, 700, 20);
        cantidadNodos.innerHTML = arbol.contarNodosRecursivo(arbol.obtenerRaiz());
    }, 3000);
};
