var ArbolBinarioAVL = function() {
    this.raiz = null;
};


ArbolBinarioAVL.prototype.obtenerRaiz = function() {
    return this.raiz;
};

ArbolBinarioAVL.prototype.asignarRaiz = function(p) {
    this.raiz = p;
};
ArbolBinarioAVL.prototype.insertar = function(dato) {
    this.insertarRecursivo(this.obtenerRaiz(), null, dato);
    this.calcFB(this.obtenerRaiz());
    this.rotar(this.obtenerRaiz(), null);
    this.calcFB(this.obtenerRaiz());

};

ArbolBinarioAVL.prototype.insertarRecursivo = function(p, ant, dato) {
    if (p != null) {
        if (dato < p.obtenerDato()) {
            this.insertarRecursivo(p.obtenerLI(), p, dato);
        };
        if (dato > p.obtenerDato()) {
            this.insertarRecursivo(p.obtenerLD(), p, dato);
        };
    } else if (ant != null) {

        if (dato < ant.obtenerDato()) {
            var q;
            q = new Nodo(dato);
            ant.asignarLI(q);
        } else if (dato > ant.obtenerDato()) {
            var q;
            q = new Nodo(dato);
            ant.asignarLD(q);
        };
    } else if (ant == null) {
        var q;
        this.raiz = q = new Nodo(dato);
    };
};

ArbolBinarioAVL.prototype.existe = function(dato) {
    return this.existeR(this.obtenerRaiz(), dato);
};

ArbolBinarioAVL.prototype.existeR = function(p, dato) {
    c = false;
    if (p != null) {
        if (p.obtenerDato() === dato) {
            c = true;
        };
        c = c || this.existeR(p.obtenerLI(), dato);
        c = c || this.existeR(p.obtenerLD(), dato);

    };
    return c;
};

ArbolBinarioAVL.prototype.buscarR = function(p, dato) {
    c = false;
    if (p != null) {
        if (dato === p.obtenerDato()) {
            c = true;
        } else if (dato < p.obtenerDato()) {
            c = c || this.buscarR(p.obtenerLI(), dato);
        } else if (dato > p.obtenerDato()) {
            c = c || this.buscarR(p.obtenerLD(), dato);
        }
    }
    return c;
};

ArbolBinarioAVL.prototype.contarHojasR = function(p) {
    var c = 0;
    if (p !== null) {
        if (p.obtenerLI() == null && p.obtenerLD() == null) {
            c++;

        };
        c = c + this.contarHojasR(p.obtenerLI());
        c = c + this.contarHojasR(p.obtenerLD());
    };
    return c;
};

ArbolBinarioAVL.prototype.contarHojas = function() {
    var c = this.contarHojasR(this.obtenerRaiz());

    return c;
};

ArbolBinarioAVL.prototype.recorridoIN = function(p) {
    var res = "";
    if (p != null) {

        res = res + this.recorridoIN(p.obtenerLI());
        res = res + p.obtenerDato();
        res = res + this.recorridoIN(p.obtenerLD());
    };
    return res + " ";
};

ArbolBinarioAVL.prototype.recorridoPOS = function(p) {
    var res = "";
    if (p != null) {

        res = res + this.recorridoPOS(p.obtenerLI());
        res = res + this.recorridoPOS(p.obtenerLD());
        res = res + p.obtenerDato();

    };
    return res + " ";
};

ArbolBinarioAVL.prototype.recorridoPRE = function(p) {
    var res = "";
    if (p != null) {

        res = res + p.obtenerDato();
        res = res + this.recorridoPRE(p.obtenerLI());
        res = res + this.recorridoPRE(p.obtenerLD());
    };
    return res;
};

ArbolBinarioAVL.prototype.alturaArbolR = function(p) {
    var c = 0;
    if (p != null) {
        c = (1 + Math.max(this.alturaArbolR(p.obtenerLI()), this.alturaArbolR(p.obtenerLD())));
    };
    return c;
};

ArbolBinarioAVL.prototype.pintarR = function(can, p, x, y) {
    var radio = 48;
    if (p !== null) {
        var delta;
        if (p.obtenerLI() != null) {
            can.strokeStyle = "#CC6B27";
            delta = (this.contarNodosIzquierdos(p) + 2) * radio;
            can.beginPath();
            can.moveTo(x, y + 20);
            can.lineTo(x - delta + 50, y + 20);
            can.lineTo(x - delta + 50, y + radio);
            can.stroke();
            can.closePath();
            this.pintarR(can, p.obtenerLI(), x - delta, y + radio);
        };
        if (p.obtenerLD() != null) {
            can.strokeStyle = "#CC6B27";
            delta = (this.contarNodosDerechos(p) + 2) * radio;
            can.beginPath();
            can.moveTo(x + 100, y + 20);
            can.lineTo(x + 50 + delta, y + 20);
            can.lineTo(x + 50 + delta, y + radio);
            can.stroke();
            can.closePath();
            this.pintarR(can, p.obtenerLD(), x + delta, y + radio);
        };
        p.pintarNodo(can, x, y);
    };
};

ArbolBinarioAVL.prototype.pintar = function(can, x, y) {
    this.pintarR(can, this.obtenerRaiz(), x, y);
};

ArbolBinarioAVL.prototype.pintarFondoNodo = function(can, p, x, y, dato) {
    var radio = 48;

    if (p != null) {
        var delta;
        if (p.obtenerLI != null) {
            delta = (this.contarNodosIzquierdos(p) + 2) * radio;
            this.pintarFondoNodo(can, p.obtenerLI(), x - delta, y + radio, dato);
        };
        if (p.obtenerLD != null) {
            delta = (this.contarNodosDerechos(p) + 2) * radio;
            this.pintarFondoNodo(can, p.obtenerLD(), x + delta, y + radio, dato);
        };
        if (p.obtenerDato() === dato) {
            p.pintarFondo(can, x, y);
        }
    };
};

ArbolBinarioAVL.prototype.calcFB = function(p) {
    var c = 0;
    if (p != null) {
        c = this.alturaArbolR(p.obtenerLI()) - this.alturaArbolR(p.obtenerLD());
        p.asignarFB(c);
        if (p.obtenerLI() != null) {
            this.calcFB(p.obtenerLI());
        };
        if (p.obtenerLD() != null) {
            this.calcFB(p.obtenerLD());
        };
    };
};

ArbolBinarioAVL.prototype.rotar = function(p, ant) {
    if (p != null) {
        if (p.obtenerFB() === 2 && p.obtenerLI().obtenerFB() === 1 && this.evaluarHijos(p) === false || p.obtenerFB() === 2 && p.obtenerLI().obtenerFB() === 0) {
            if (ant == null) {
                this.asignarRaiz(this.rotDer(p, p.obtenerLI()));
            } else if (ant.obtenerLI() === p) {
                ant.asignarLI(this.rotDer(p, p.obtenerLI()));
            } else {
                ant.asignarLD(this.rotDer(p, p.obtenerLI()));
            };
            this.calcFB(this.obtenerRaiz());
        } else if (p.obtenerFB() === -2 && p.obtenerLD().obtenerFB() === -1 && this.evaluarHijos(p) === false || p.obtenerFB() === -2 && p.obtenerLD().obtenerFB() == 0) {
            if (ant == null) {
                this.asignarRaiz(this.rotIzq(p, p.obtenerLD()));
            } else if (ant.obtenerLI() === p) {
                ant.asignarLI(this.rotIzq(p, p.obtenerLD()));
            } else {
                ant.asignarLD(this.rotIzq(p, p.obtenerLD()));
            };
            this.calcFB(this.obtenerRaiz());
        } else if (p.obtenerFB() === 2 && p.obtenerLI().obtenerFB() === -1 && this.evaluarHijos(p) === false) {
            var q = p.obtenerLI();
            var r = this.hijoMaxAlt(q);

            if (ant == null) {
                this.asignarRaiz(this.rotDobleDere(p, q, r));
            } else if (ant.obtenerLI() === p) {
                ant.asignarLI(this.rotDobleDere(p, q, r));
            } else {
                ant.asignarLD(this.rotDobleDere(p, q, r));
            };
            this.calcFB(this.obtenerRaiz());
        } else if (p.obtenerFB() === -2 && p.obtenerLD().obtenerFB() === 1 && this.evaluarHijos(p) === false) {
            var q = p.obtenerLD();
            var r = this.hijoMaxAlt(q);

            if (ant == null) {
                this.asignarRaiz(this.rotDobleIzq(p, q, r));
            } else if (ant.obtenerLD() === p) {
                ant.asignarLD(this.rotDobleIzq(p, q, r));
            } else {
                ant.asignarLI(this.rotDobleIzq(p, q, r));
            };
            this.calcFB(this.obtenerRaiz());
        }

        if (p.obtenerLI() != null) {
            this.rotar(p.obtenerLI(), p);
        };
        if (p.obtenerLD() != null) {
            this.rotar(p.obtenerLD(), p);
        };

    };
};

ArbolBinarioAVL.prototype.rotDer = function(p, q) {
    p.asignarLI(q.obtenerLD());
    q.asignarLD(p);

    return q;
};

ArbolBinarioAVL.prototype.rotIzq = function(p, q) {
    p.asignarLD(q.obtenerLI());
    q.asignarLI(p);

    return q;
};

ArbolBinarioAVL.prototype.rotDobleDere = function(p, q, r) {
    q.asignarLD(r.obtenerLI());
    p.asignarLI(r.obtenerLD());
    r.asignarLI(q);
    r.asignarLD(p);

    return r;
};

ArbolBinarioAVL.prototype.rotDobleIzq = function(p, q, r) {
    q.asignarLI(r.obtenerLD());
    p.asignarLD(r.obtenerLI());
    r.asignarLD(q);
    r.asignarLI(p);
    return r;
};

ArbolBinarioAVL.prototype.hijoMaxAlt = function(q) {
    var a = this.alturaArbolR(q.obtenerLI());
    var b = this.alturaArbolR(q.obtenerLD());
    if (a > b) {
        return q.obtenerLI();
    } else {
        return q.obtenerLD();
    };
};

ArbolBinarioAVL.prototype.limpiarFondo = function(can, x, y) {
    can.clearRect(x, y, 3000, 3000);
};

ArbolBinarioAVL.prototype.contarNodosDerechos = function(p) {
    var c = 0;
    if (p.obtenerLI() != null) {
        c = this.contarNodosRecursivo(p.obtenerLI());
    };
    return c;
};

ArbolBinarioAVL.prototype.contarNodosIzquierdos = function(p) {
    var c = 0;
    if (p.obtenerLD() != null) {
        c = this.contarNodosRecursivo(p.obtenerLD());
    };
    return c;
};

ArbolBinarioAVL.prototype.contarNodosRecursivo = function(p) {
    var c = 0;
    if (p != null) {
        c += this.contarNodosRecursivo(p.obtenerLI());
        c += this.contarNodosRecursivo(p.obtenerLD());
        c++;
    };
    return c;
};

ArbolBinarioAVL.prototype.evaluarFBHijos = function(p) {
    var c = false;
    if (p !== null) {
        if (p.obtenerFB() === 2 || p.obtenerFB() === -2) {
            c = true;
        };
        c = c || this.evaluarFBHijos(p.obtenerLI());
        c = c || this.evaluarFBHijos(p.obtenerLD());
    };
    return c;
};

ArbolBinarioAVL.prototype.evaluarHijos = function(p) {
    var c = this.evaluarFBHijos(p.obtenerLI()) || this.evaluarFBHijos(p.obtenerLD());
    return c;
};

ArbolBinarioAVL.prototype.eliminarRecursivo = function(p, ant, dato) {
    if (p !== null) {
        if (p.obtenerDato() === dato) {
            if (p.obtenerLI() == null && p.obtenerLD() == null) {
                //el nodo a borrar es una hoja
                if (ant == null) {
                    //el nodo a borrar es la raiz del árbol
                    this.asignarRaiz(null);
                } else if (ant.obtenerLI() === p) {
                    ant.asignarLI(null);
                } else {
                    ant.asignarLD(null);
                };
            } else {
                if (this.alturaArbolR(p.obtenerLI()) < this.alturaArbolR(p.obtenerLD())) {
                    NRaiz = this.nuevaRaizDer(p.obtenerLD(), p);
                    if (NRaiz === p.obtenerLI() || NRaiz === p.obtenerLD()) {
                        NRaiz.asignarLI(p.obtenerLI());
                    } else {
                        NRaiz.asignarLI(p.obtenerLI());
                        NRaiz.asignarLD(p.obtenerLD());
                    };
                    if (ant == null) {
                        this.asignarRaiz(NRaiz);
                        //p=null;
                    } else if (ant.obtenerLI() === p) {
                        ant.asignarLI(NRaiz);
                    } else {
                        ant.asignarLD(NRaiz);
                    };
                } else {
                    NRaiz = this.nuevaRaizIzq(p.obtenerLI(), p);
                    if (NRaiz === p.obtenerLI() || NRaiz === p.obtenerLD()) {
                        NRaiz.asignarLD(p.obtenerLD());
                    } else {
                        NRaiz.asignarLI(p.obtenerLI());
                        NRaiz.asignarLD(p.obtenerLD());
                    };
                    if (ant == null) {
                        this.asignarRaiz(NRaiz);
                    } else if (ant.obtenerLI() === p) {
                        ant.asignarLI(NRaiz);
                    } else {
                        ant.asignarLD(NRaiz);
                    };
                };
            };
        };
        if (dato < p.obtenerDato()) {
            this.eliminarRecursivo(p.obtenerLI(), p, dato);
        };
        if (dato > p.obtenerDato()) {
            this.eliminarRecursivo(p.obtenerLD(), p, dato);
        };
    };
};

ArbolBinarioAVL.prototype.eliminar = function(dato) {
    this.eliminarRecursivo(this.obtenerRaiz(), null, dato);
    this.calcFB(this.obtenerRaiz());
    this.rotar(this.obtenerRaiz(), null);
    this.calcFB(this.obtenerRaiz());
};

ArbolBinarioAVL.prototype.nuevaRaizIzq = function(p, antP) {
    //busca el nodo con el mayor dato del subárbol
    var q, z;
    while (p != null) {
        z = q;
        q = p;
        p = p.obtenerLD();
    }
    if (q.obtenerLI() == null && q.obtenerLD == null) {
        if (z == null) {
            z = antP;
        }
        z.asignarLD(null);
    } else if (z == null && antP != null) {
        //si la nueva raiz del subarbol es uno de sus hijos
        z = antP;
        q.asignarLD(z.obtenerLD());
    } else {
        z.asignarLD(q.obtenerLI());
    }
    return q;
};
//Hay un error al leer la propiedad si entra y 'z' es igual a 'q' y 'q' es igual a null
ArbolBinarioAVL.prototype.nuevaRaizDer = function(p, antP) {
    //busca el nodo con el menor dato del subárbol
    var q, z;
    while (p != null) {
        z = q;
        q = p;
        p = p.obtenerLI();
    }
    if (q.obtenerLI() == null && q.obtenerLD() == null) {
        if (z === null) {
            z = antP;
        }
        z.asignarLI(null);
    } else if (z == null && antP != null) {
        //si la nueva raiz del subarbol es uno de sus hijos
        z = antP;
        q.asignarLI(z.obtenerLI());
    } else {
        z.asignarLI(q.obtenerLD());
    }
    return q;
};
//Tengo que evaluar si es posible asignar una raiz nueva buscando los nodos mas internos del arbol si este no es el caso toca bucar otra forma de hacerlo

ArbolBinarioAVL.prototype.padreNRaiz = function(p) {
    var q, z;
    while (p != null) {
        z = q;
        q = p;
        p = p.obtenerLD();
    };
    return z;
};
