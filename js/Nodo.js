var Nodo = function(d) {
    var dato;
    var LI;
    var LD;
    var FB;
    var anchoNodo = 100;
    var altoNodo = 40;

    this.directorio = new Directorio(100);
    this.direccion = this.directorio.pedirDireccion();
    this.asignarDato(d);
    this.asignarFB(0);
    this.asignarLI(null);
    this.asignarLD(null);

};

Nodo.prototype.asignarDato = function(d) {
    this.dato = d;
};

Nodo.prototype.obtenerDato = function() {
    return this.dato;
};

Nodo.prototype.asignarLI = function(x) {
    this.LI = x;
};

Nodo.prototype.obtenerLI = function() {
    return this.LI;
};

Nodo.prototype.asignarLD = function(y) {
    this.LD = y;
};

Nodo.prototype.obtenerLD = function() {
    return this.LD;
};

Nodo.prototype.asignarFB = function(y) {
    this.FB = y;
};

Nodo.prototype.obtenerFB = function() {
    return this.FB;
};

Nodo.prototype.pintarNodo = function(can, x, y) {

    can.beginPath();
    can.strokeStyle = "#77B830";
    can.fillStyle = "#000";
    can.strokeRect(x, y, 100, 40);

    can.moveTo(x + 30, y);
    can.lineTo(x + 30, y + 40);

    can.moveTo(x + 70, y);
    can.lineTo(x + 70, y + 40);

    can.font = "20px Arial";
    can.fillText(this.obtenerDato(), x + 40, y + 30);

    if (this.obtenerLI() === null) {
        can.font = "15px Arial";
        can.fillText("null", x + 5, y + 25);
    } else {
        can.font = "15px Arial";
        can.fillText(this.obtenerLI().direccion, x + 5, y + 25);
    }

    if (this.obtenerLD() === null) {
        can.font = "15px Arial";
        can.fillText("null", x + 75, y + 25);
    } else {
        can.font = "15px Arial";
        can.fillText(this.obtenerLD().direccion, x + 75, y + 25);
    }

    can.font = "10px Arial";
    can.fillText(this.direccion, x, y - 3);

    can.font = "10px Arial";
    can.fillText("FB:" + this.FB, x + 80, y - 3);

    can.stroke();
    can.closePath();
};

Nodo.prototype.pintarFondo = function(can, x, y) {
    can.beginPath();
    can.fillStyle = "#63FFDE";
    can.fillRect(x, y, 100, 40);
    can.stroke();
    can.closePath();
};
