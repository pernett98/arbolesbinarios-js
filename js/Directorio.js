var Directorio = function(x) {
    this.N = x;
    this.vdir = [100];
    this.ndir = 0;
};

Directorio.prototype.pedirDireccion = function() {
    var n;
    while (this.dirUtilizada(this.n = Math.floor(Math.random() * 100))) {}
    this.vdir.push(this.n);
    return this.n;
};

Directorio.prototype.dirUtilizada = function(x) {
    var respuesta;
    for (var i = 0; !(i >= this.ndir || respuesta); ++i) {
        if (this.vdir[i] !== x) {
            respuesta = true;
        };
    };
};
